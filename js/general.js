// JavaScript Document

(function(){
	"use strict";
	var filterPanel = $(".filter-sp-panel"),
		filterOpen = $(".filter-SP"),
		filterClose = $(".filter-close"),
		count = 0,
		timeBtn = $(".time-slot button"),
		msgLabel = $(".msg-label"),
		msgSp = $(".msg-sp"),
		phaseOne = $(".phase-one"),
		phaseTwo = $(".phase-two"),
		anotherService = $(".another-service"),
		addService = $(".add-service"),
		backToPhaseOne = $(".back-to-phase1");
	
	filterOpen.click(function(){
		filterPanel.addClass("show-slide");
	});
	
	filterClose.click(function(){
		filterPanel.removeClass("show-slide");
	});
	
	timeBtn.click(function(){
		if (count === 0){
			$(this).removeClass("btn-outline-secondary").addClass("btn-outline-success");
			count = 1;
		} else {
			$(this).addClass("btn-outline-secondary").removeClass("btn-outline-success");
			count = 0;
		}
	});
	
	msgLabel.click(function(){
		if (count === 0){
			msgSp.addClass("msg-sp-height");
			count = 1;
		} else {
			msgSp.removeClass("msg-sp-height");
			count = 0;
		}
	});
	
	anotherService.click(function(){
		phaseOne.addClass("d-none");
		phaseTwo.removeClass("d-none");
	});
	
	addService.click(function(){
		phaseOne.removeClass("d-none");
		phaseTwo.addClass("d-none");
	});
	
	backToPhaseOne.click(function(){
		phaseOne.removeClass("d-none");
		phaseTwo.addClass("d-none");
	});
	
}());